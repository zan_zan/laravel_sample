<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Input;
use Redirect;
use View;


class ProductController extends Controller
{
	protected $rules=[
		'name' =>['required','min:3'],
		'slug' =>['required'],
	];


    public function index()
    {
    	$products = Product::paginate(5);
    	return View::make('products.index',compact('products'));
    }


    public function create()
    {
    	return view('products.create');
    }


    public function store(Request $request)
    {
    	$this->validate($request,$this->rules);

    	//$input = Input::all();
        $destinationPath = 'img/';
           $filename = Input::file('image')->getClientOriginalName();
           // var_dump($filename);
       $img = Input::file('image')->move($destinationPath,$filename);
       //var_dump($img);

            Product::create([
                 'name' => Input::get('name'),
                 'code' => Input::get('code'),
                 'image' => $img,
                 'slug' => Input::get('slug'),
                 ]);
       //Product::create($input);
       
        return Redirect::route('products.index')->with('message','Product added!');
    }


    public function show(Product $product)
    {
    	return view('products.show',compact('product'));
    }


    public function edit(Product $product)
    {
    	return view('products.edit',compact('product'));
    }


    public function update(Product $product,Request $request)
    {
    	$this->validate($request,$this->rules);
        $product->name =Input::get('name');
        $product->code =Input::get('code');
        $product->slug = Input::get('slug');
    	//$input = array_except(Input::all(),'_method');
    	//$product->update($input);
        if(Input::hasFile('image')){
            $destinationPath = 'img/';
            $filename = Input::file('image')->getClientOriginalName();
            $img = Input::file('image')->move($destinationPath,$filename);
            $product->image = $img;
        }
       $product->save();

         // Product::edit([
         //    'name' => Input::get('name'),
         //    'code' => Input::get('code'),
         //    'image' => $img,
         //    'slug' => Input::get('slug'),
         //    ]);

    	return Redirect::route('products.index',$product->slug);
    }


    public function destroy(Product $product)
    {
    	$product->delete();

    	return Redirect::route('products.index');
    }

}
