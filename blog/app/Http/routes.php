<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('products','ProductController@paging');

Route::model('products', 'Product');

$router->bind('products',function($slug,$route){

	return App\Product::whereSlug($slug)->first();

});

Route::get('products/paging','ProductController@paging');

$router->resource('products','ProductController');
