@extends('layout')

@section('content')

  <h2>Edit Product</h2>
  {!! Form::model($product,['method'=>'PATCH','files'=>true,'route'=>['products.update',$product->slug]]) !!}
    @include('products/form',['submit_text'=>'Edit Product'])
  {!! Form::close() !!}

  @endsection