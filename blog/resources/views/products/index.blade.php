@extends('layout')

@section('content')
<h2>Product List</h2>

{!! link_to_route('products.create','Create Product') !!}

  <table class="table table-bordered">
        <thhead>
        	<tr>
        		<th>Name</th>
        		<th>Code</th>
        		<th>Image</th>
        		<th>-</th>
        		<th>-</th>
        	</tr>
        </thhead>
        <tbody>
        	@foreach($products as $product)
        	<tr>
        	 {!! Form::open(array('class'=>'form-inline','method'=>'DELETE','route'=>array('products.destroy',$product->slug))) !!}
        		<td>{{ $product->name }}</td>
        		<td>{{ $product->code }}</td>
        		<td><img src="/{{ $product->image }}" width="20px"></td>
        		<td>{!! link_to_route('products.edit','Edit',array($product->slug),array('class'=>'btn btn-info')) !!}</td>
        		<td>{!! Form::submit('Delete',array('class'=>'btn btn-danger')) !!}</td>
 			 {!! Form::close() !!}
        	</tr>
        	@endforeach
        </tbody>
  </table>
  <div class="pagination"> {!! $products->render() !!} </div>

  

@endsection