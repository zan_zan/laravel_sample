@extends('layout')
@section('content')

<h2>Create New Product</h2>
{!! Form::open(array('route'=>'products.store','files'=>true,'method'=>'post')) !!}
<!-- {!! Form::model(new App\Product,['route'=>['products.store']]) !!} -->
    @include('products/form',['submit_text'=> 'Create Product'])
{!! Form::close() !!}

@if ($errors->any())
	<ul class="alert alert-danger">

		@foreach ($errors->all() as $error)

			<li>{{ $error }}</li>

		@endforeach

	</ul>
@endif

@endsection