<div class="form-group">
  {!! Form::label('name','Name:') !!}
  {!! Form::text('name') !!}
</div>
<div class="form-group">
  {!! Form::label('code','Code:') !!}
  {!! Form::text('code') !!}
</div>
<div class="form-group">
  {!! Form::label('image','Image:') !!}
  {!! Form::file('image') !!}
</div>
<div class="form-group">
   {!! Form::label('slug','Slug:') !!}
   {!! Form::text('slug') !!}
</div>
<div class="form-group">
	{!! Form::submit($submit_text,['class'=>'btn primary']) !!}
</div>